/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-core', [ //
	'am-wb-core', // 
	'seen-core',
])
	
/*
 * Load widgets
 */
.run(function ($widget) {
    $widget.newWidget({
        // widget description
        type: 'ObjectCollection',
        title: 'Object collection',
        description: 'A widget to show a collection of items',
        groups: ['seen'],
        icon: 'pages',
        model: '',
        // functional properties
        help: '',
        helpId: 'wb-seen-widget-collection',
        template: '<div></div>',
        controller: 'AmWbSeenCollectionWidget'
    });
    
    $widget.newWidget({
        type: 'import',
        title: 'Import',
        description: 'Import a part of other content',
        groups: ['commons'],
        icon: 'import_export',
        setting: ['import'],
        // help
        help: '',
        helpId: '',
        // functional (page)
        template: '<div></div>',
        controller: 'WbWidgetSeenImport'
    });
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('am-wb-seen-core')
/**
 * @ngdoc Widgets
 * @name collection
 * @description Collection controller
 * 
 * A widget collection controller
 * 
 */
.factory('AmWbSeenCollectionWidget', function (WbWidgetGroup, $q, $http, QueryParameter, $wbUtil) {
	'use strict';

	/*
	 * process state
	 */
	var STATE_BUSY = 'busy';
	var STATE_IDEAL = 'ideal';
	var collectionAttributes = ['url', 'filters', 'sorts', 'query', 'properties', 'template'];

	// ------------------------------------------------------------------
	// Utility
	// ------------------------------------------------------------------

	/*
	 * Creates widgets based on a mix of the template and input data
	 * 
	 * #utility
	 */
	function createWidgets(items, template) {
		var widgetList = [];
		var templateCopy = '';
		var html;
		if (!angular.isArray(items)) {
			return $q.resolve([]); // Empty items?
		}
		for (var i = 0; i < items.length; i++) {
			templateCopy = template;
			try {
				html = Mustache.to_html(templateCopy, items[i]);
				html = JSON.parse(html);
				html = $wbUtil.clean(html);
				widgetList.push(html);
			} catch (e) {
				console.error({
					message: 'Falie to load template',
					error: e
				});
				continue;
			}
		}
		return $q.resolve(widgetList);
	}

	// ------------------------------------------------------------------
	// Widget internal
	//
	// ------------------------------------------------------------------
	/*
	 * TODO: maso, 2018: manage events 
	 * 
	 * - collection changes: any part of the query is changed 
	 * - style changes: internal group style changed 
	 * - model changes: the whole model changed
	 */

	function Widget($scope, $element, $parent){
		WbWidgetGroup.apply(this, [$scope, $element, $parent]);
		this.setAllowedTypes();
		this.addElementAttributes('url', 'filters', 'sorts', 'query', 'properties', 'template');

		this._lastResponse;
		this._state = STATE_IDEAL;
		var ctrl = this;

		// watch model update
		function doTask ($event) {
			// collection updated
			if (_.includes(collectionAttributes, $event.key)) {
				ctrl.reloadPage();
			}
		}

		ctrl.on('modelUpdated', doTask);
		ctrl.on('runtimeModelUpdated', doTask);
		this.on('loaded', function(){
			ctrl.reloadPage();
		});
	};
	Widget.prototype = Object.create(WbWidgetGroup.prototype);

	/**
	 * Gets collection from server, creates widgets, and forms the body of widget
	 * 
	 * @memberof AmWbSeenCollection
	 */
	Widget.prototype.reloadPage = function () {
		var ctrl = this;
		if(this._reloading){
			return this._reloading.finally(function(){
				return ctrl.reloadPage();
			});
		}
		this.removeChildren();
		this.getElement().empty();
		delete this._lastResponse;
		this._reloading = this.loadNextPage(true)
		.finally(function(){
			delete ctrl._reloading;
		});
	};


	/**
	 * Load next page 
	 * 
	 * @memberof AmWbSeenCollection
	 * @param replace {boolean} current items or not
	 * @returns {number} the number of items in each page
	 */
	Widget.prototype.loadNextPage = function () {
		if (!this.hasMorePage()) {
			return $q.reject({
				message: 'No more page!?'
			});
		}
		var template = this.getTemplate();

		var ctrl = this;
		return this.getCollection()//
		.then(function (res) {
			ctrl._lastResponse = res.data;
			return ctrl.fire('success', res) || res.data;
		}, function (error) {
			return ctrl.fire('error', error) || error;
		})
		.then(function (data) {
			return createWidgets(data.items || [], template);
		})//
		.then(function (children) {
			return ctrl.addChildren(ctrl.getChildren().length, children)
			.then(function(){
				return ctrl.fire('load', {
					children: children
				}) || children;
			});
		});
	};

	Widget.prototype.getTemplate = function(){
		return this.getProperty('template') || this.getModelProperty('template') || {
			type: 'HtmlText',
			text: '<h3>Template is not set</h3>'
		};
	}

	/*
	 * Gets collection based on internal configurations.
	 * 
	 * This is an internal function
	 */
	Widget.prototype.getCollection = function () {
		// check state
		if (this._state !== STATE_IDEAL) {
			return this.lastQuery;
		}

		var q = new QueryParameter();

		//TODO: maso, 2019: merge runtime and origin model into a new model
		// like the way used in 'loadStyle' function in 'widgets-ctrl' in am-wb-core module

		// filters
		var filters = this.getProperty('filters') || this.getModelProperty('filters') || [];
		angular.forEach(filters, function (filter) {
			q.addFilter(filter.key, filter.value);
		});

		// sort
		var sorts = this.getProperty('sorts') || this.getModelProperty('sorts') || [];
		angular.forEach(sorts, function (sort) {
			q.addSorter(sort.key, sort.order);
		});

		q.setQuery(this.getProperty('query') || this.getModelProperty('query'));
		q.put('graphql', this.getProperty('properties') || this.getModelProperty('properties'));
		var url = this.getProperty('url') || this.getModelProperty('url');
		if (url) {
			var pageIndex = this.getNextPageIndex();
			if(pageIndex > 1){
				q.setPage(pageIndex);
			}
			this._state = STATE_BUSY;
			var ctrl = this;
			this._lastQuery = $http({
				method: 'GET',
				url: url,
				params: q.getParameter()
			})
			.finally(function () {
				ctrl._state = STATE_IDEAL;
				delete ctrl.lastQuery;
			});
		} else {
			this._lastQuery = $q.reject({
				message: 'URL is not set',
				code: 1 // url is not set
			});
		}
		return this._lastQuery;
	};



	// ------------------------------------------------------------------
	// Widget global
	// ------------------------------------------------------------------

	/**
	 * Gets counts of all pages
	 * 
	 * @memberof AmWbSeenCollection
	 * @returns {number} the number of pages 
	 */
	Widget.prototype.getPagesCount = function () {
		if (!this._lastResponse) {
			return 0;
		}
		return this._lastResponse.page_number;
	};

	/**
	 * Gets current page number
	 * 
	 * @memberof AmWbSeenCollection
	 * @returns {number} the current page
	 */
	Widget.prototype.getCurrentPage = function () {
		if (!this._lastResponse) {
			return 0;
		}
		return this._lastResponse.current_page;
	};

	/**
	 * The number of items per each page
	 * 
	 * @memberof AmWbSeenCollection
	 * @returns {number} the number of items in each page
	 */
	Widget.prototype.getItemPerPage = function () {
		if (!this._lastResponse) {
			return 0;
		}
		return this._lastResponse.items_per_page;
	};

	/**
	 * Check if there is more page
	 * 
	 * @memberof AmWbSeenCollection
	 * @returns {boolean} true if there is more page
	 */
	Widget.prototype.hasMorePage = function () {
		if (!this._lastResponse) {
			return true;
		}
		return this._lastResponse.current_page < this._lastResponse.page_number;
	};

	/**
	 * Gets next page index
	 * 
	 * @memberof AmWbSeenCollection
	 * @returns {number} next page index
	 */
	Widget.prototype.getNextPageIndex = function () {
		if (!this._lastResponse) {
			return 1;
		}
		return this._lastResponse.current_page + 1;
	};

	/**
	 * The current state of the controller
	 * 
	 * @memberof AmWbSeenCollection
	 * @returns {string} current state of the controller
	 */
	Widget.prototype.getState = function () {
		return this._state || STATE_IDEAL;
	};


	/**
	 * set acceptable widgets
	 * 
	 * $widget.setAcceptableChild('a', 'b');
	 * 
	 * @memberof WbWidgetGroupCtrl
	 */
	Widget.prototype.setAllowedTypes = function () {
		this.allowedTypes = [];
	};

	/**
	 * Set edit mode
	 * 
	 * 
	 * @memberof WbAbstractWidget
	 */
	Widget.prototype.setEditable = function (editable) {
		WbWidgetGroup.prototype.setEditable.apply(this, arguments);
		// propagate to child
		var children = this.getChildren();
		while(!_.isEmpty(children)){
			var widget = children.pop();
			widget.setSilent(editable);
			if(!widget.isLeaf()){
				children = children.concat(widget.getChildren());
			}
		}
	};


	Widget.prototype.isLeaf = function(){
		return true;
	};

	return Widget;
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('am-wb-core')//

/**
 * @ngdoc Widgets
 * @name import
 * @description Manage a widget
 */
.factory('WbWidgetSeenImport', function (WbWidgetGroup, $wbUtil, $q) {
    'use strict';

    /*
     * Load data from the widget URL
     */
    function loadLinks(paths) {

        var jobs = [];
        var models = Array(paths.length);
        _.forEach(paths, function(path, index){
            // check parts
            var parts = path.split("#");
            var url = parts[0];
            var id = parts.length > 1 ? parts[1] : undefined;
            var job = $wbUtil.downloadWidgetModel(url, id)
            .then(function (model) {
                models[index] = model;
            });
            jobs.push(job);
        });

        function clean(){
            return _.remove(models, function(model) {
                return !_.isUndefined(model);
            });
        }
        // TODO: maso, 2019: some models are faild
        return $q.all(jobs)
        .then(clean,clean);
    };

    //-------------------------------------------------------------
    // Widget
    //-------------------------------------------------------------
    function Widget($scope, $element, $parent){
        WbWidgetGroup.apply(this, [$scope, $element, $parent]);
        this.setAllowedTypes();
        this.addElementAttributes('url');

        // load widget
        var ctrl = this;
        function checkAndUpdateUrl($event) {
            if ($event.key === 'url') {
                ctrl.reload()
            }
        }
        this.on('modelUpdated', checkAndUpdateUrl);
        this.on('runtimeModelUpdated', checkAndUpdateUrl);
        this.on('modelChanged', function () {
            ctrl.reload();
        });
    };
    Widget.prototype = Object.create(WbWidgetGroup.prototype);

    Widget.prototype.reload = function (){
        // check if the url
        var ctrl = this;
        ctrl.removeChildren();
        ctrl.getModel().children = [];
        var path = this.getProperty('url') || this.getModelProperty('url') || '';
        path = path.trim();
        var prom;
        if(!path){
            prom = $q.resolve([]);
        } else {
            this.paths = path.split(',');
            prom = loadLinks(this.paths)//
            .then(function (children) {
                return ctrl.fire('success', {
                    children: children
                }) || children;
            });
        }

        return prom.then(function(children){
            return ctrl.addChildrenModel(0, children)
            .finally(function(){
                ctrl.fire('load', {
                    children: children
                }) || children;
            });
        });
    };

    /**
     * set acceptable widgets
     * 
     * $widget.setAcceptableChild('a', 'b');
     * 
     * @memberof WbWidgetGroupCtrl
     */
    Widget.prototype.setAllowedTypes = function () {
        this.allowedTypes = [];
    };


    /**
     * Set edit mode
     * 
     * 
     * @memberof WbAbstractWidget
     */
    Widget.prototype.setEditable = function (editable) {
        WbWidgetGroup.prototype.setEditable.apply(this, arguments);
        // propagate to child
        var children = this.getChildren();
        while(!_.isEmpty(children)){
            var widget = children.pop();
            widget.setSilent(editable);
            if(!widget.isLeaf()){
               children = children.concat(widget.getChildren());
            }
        }
    };
    

    Widget.prototype.isLeaf = function(){
        return true;
    };

    return Widget;
});
