/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-core', [ //
	'am-wb-core', // 
	'seen-core',
])
	
/*
 * Load widgets
 */
.run(function ($widget) {
    $widget.newWidget({
        // widget description
        type: 'ObjectCollection',
        title: 'Object collection',
        description: 'A widget to show a collection of items',
        groups: ['seen'],
        icon: 'pages',
        model: '',
        // functional properties
        help: '',
        helpId: 'wb-seen-widget-collection',
        template: '<div></div>',
        controller: 'AmWbSeenCollectionWidget'
    });
    
    $widget.newWidget({
        type: 'import',
        title: 'Import',
        description: 'Import a part of other content',
        groups: ['commons'],
        icon: 'import_export',
        setting: ['import'],
        // help
        help: '',
        helpId: '',
        // functional (page)
        template: '<div></div>',
        controller: 'WbWidgetSeenImport'
    });
});

