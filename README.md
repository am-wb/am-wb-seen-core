# Angular Material Weburger Seen-Core

master: 
[![pipeline status - develop](https://gitlab.com/am-wb/am-wb-seen-core/badges/master/pipeline.svg)](https://gitlab.com/am-wb/am-wb-seen-core/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a60dab7f80d34322840d7c603f831980)](https://www.codacy.com/app/am-wb/am-wb-seen-core?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=am-wb/am-wb-seen-core&amp;utm_campaign=Badge_Grade)


See more details in [technical document](https://am-wb.gitlab.io/am-wb-seen-core/doc/index.html).