/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('WbWidget import ', function () {
	// instantiate service
	var $rootScope;
	var $widget;
	var $httpBackend;

	// load the service's module
	beforeEach(function(){
		module('am-wb-seen-core')
		inject(function (_$rootScope_, _$widget_, _$httpBackend_) {
			$rootScope = _$rootScope_;
			$widget = _$widget_;
			$httpBackend = _$httpBackend_;
		});
	});

	it('should be added at runtime', function (done) {
		var model = {
				type: 'import',
				id: 'obj1',
		};
		$widget.compile(model)
		.then(function(widget){
			expect(widget.getElementAttribute('id')).toBe(model.id);
			done();
		});
		$rootScope.$apply();
	});

	it('should load a document from url', function (done) {
		var model = {
				type: 'import',
				url: '/contents/value'
		};
		$widget.compile(model)
		.then(function(widget){
			widget.on('load', function(){
				expect(widget.getChildren().length).toBe(1);
				done();
			});
		});
		$httpBackend
		.expect('GET', model.url)
		.respond(200, {
			type: 'h1',
			html: 'test header 1'
		});
		$rootScope.$apply();
		expect($httpBackend.flush).not.toThrow();
	});

	it('should load a document from url', function (done) {
		var model = {
				type: 'import',
				url: '/contents/value'
		};
		$widget.compile(model)
		.then(function(widget){
			widget.on('load', function(){
				expect(widget.getChildren().length).toBe(1);
				done();
			});
		});
		$httpBackend
		.expect('GET', model.url)
		.respond(200, {
			type: 'h1',
			html: 'test header 1'
		});
		$rootScope.$apply();
		expect($httpBackend.flush).not.toThrow();
	});

	it('should load even if id not found', function (done) {
		var model = {
				type: 'import',
				url: '/contents/value#xxx'
		};
		$widget.compile(model)
		.then(function(widget){
			widget.on('load', function(){
				expect(widget.getChildren().length).toBe(0);
				done();
			});
		});
		$httpBackend
		.expect('GET', '/contents/value')
		.respond(200, {
			type: 'h1',
			html: 'test header 1'
		});
		$rootScope.$apply();
		expect($httpBackend.flush).not.toThrow();
	});

	it('should load a document from url', function (done) {
		var model = {
				type: 'import',
				url: '/contents/value1#h1,/contents/value2#h2'
		};
		$widget.compile(model)
		.then(function(widget){
			widget.on('load', function(){
				expect(widget.getChildren().length).toBe(2);
				expect(widget.getChildren()[0].getId()).toBe('h1');
				expect(widget.getChildren()[1].getId()).toBe('h2');
				done();
			});
		});
		$httpBackend
		.expect('GET', '/contents/value1')
		.respond(200, {
			type: 'div',
			children:[{
				type: 'h1',
				id: 'h1',
				html: 'test header 1'
			}]
		});
		$httpBackend
		.expect('GET', '/contents/value2')
		.respond(200, {
			type: 'div',
			children:[{
				type: 'h2',
				id: 'h2',
				html: 'test header 2'
			}]
		});
		$rootScope.$apply();
		expect($httpBackend.flush).not.toThrow();
	});
	

	fit('should clean old widget and load a document from url', function (done) {
		var model = {
				type: 'import',
				url: '/contents/value1#h1,/contents/value2#h2',
				children:[{
					type: 'h1',
					id: 'h1',
					html: 'test header 1'
				},{
					type: 'h1',
					id: 'h1',
					html: 'test header 1'
				},{
					type: 'h1',
					id: 'h1',
					html: 'test header 1'
				}]
		};
		$widget.compile(model)
		.then(function(widget){
			widget.on('load', function(){
				expect(widget.getChildren().length).toBe(2);
				expect(widget.getChildren()[0].getId()).toBe('h1');
				expect(widget.getChildren()[1].getId()).toBe('h2');
				done();
			});
		});
		$httpBackend
		.expect('GET', '/contents/value1')
		.respond(200, {
			type: 'div',
			children:[{
				type: 'h1',
				id: 'h1',
				html: 'test header 1'
			}]
		});
		$httpBackend
		.expect('GET', '/contents/value2')
		.respond(200, {
			type: 'div',
			children:[{
				type: 'h2',
				id: 'h2',
				html: 'test header 2'
			}]
		});
		$rootScope.$apply();
		expect($httpBackend.flush).not.toThrow();
	});

	it('should load on runtime url change', function (done) {
		var model = {
				type: 'import',
		};
		$widget.compile(model)
		.then(function(widget){
			function load1(){
				widget.off('load', load1);
				widget.on('load', load2);
				widget.setProperty('url', '/contents/value1#h1,/contents/value2#h2');
			}
			function load2(){
				expect(widget.getChildren().length).toBe(2);
				expect(widget.getChildren()[0].getId()).toBe('h1');
				expect(widget.getChildren()[1].getId()).toBe('h2');
				done();
			}
			widget.setProperty('url', '');
			widget.on('load', load1);
		});
		$httpBackend
		.expect('GET', '/contents/value1')
		.respond(200, {
			type: 'div',
			children:[{
				type: 'h1',
				id: 'h1',
				html: 'test header 1'
			}]
		});
		$httpBackend
		.expect('GET', '/contents/value2')
		.respond(200, {
			type: 'div',
			children:[{
				type: 'h2',
				id: 'h2',
				html: 'test header 2'
			}]
		});
		$rootScope.$apply();
		expect($httpBackend.flush).not.toThrow();
	});
});
