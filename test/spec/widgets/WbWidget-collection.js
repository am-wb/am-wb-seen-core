/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('WbWidget ObjectCollection ', function () {
    // instantiate service
    var $rootScope;
    var $widget;
    var $httpBackend;

    // load the service's module
    beforeEach(function(){
        module('am-wb-seen-core')
        inject(function (_$rootScope_, _$widget_, _$httpBackend_) {
            $rootScope = _$rootScope_;
            $widget = _$widget_;
            $httpBackend = _$httpBackend_;
        });
    });

    it('should be added at runtime', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
        };
        $widget.compile(model)
        .then(function(widget){
            expect(widget.getElementAttribute('id')).toBe(model.id);
            done();
        });
        $rootScope.$apply();
    });

    it('should load template from url', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                template: '{"type":"h1", "html":"{{title}}"}',

        };
        $widget.compile(model)
        .then(function(widget){
            expect(widget.getElementAttribute('id')).toBe(model.id);
            widget.on('load', function(){
                expect(widget.getChildren().length).toBe(3);
                done();
            });
        });
        $httpBackend
        .expect('GET', model.url)
        .respond(200, {
            items: [{
                title: 'text1'
            },{
                title: 'text2'
            },{
                title: 'text3'
            }]
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });

    it('should load non SEEN list response', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                template: '{"type":"h1", "html":"{{title}}"}',

        };
        $widget.compile(model)
        .then(function(widget){
            expect(widget.getElementAttribute('id')).toBe(model.id);
            widget.on('load', function(){
                expect(widget.getChildren().length).toBe(0);
                done();
            });
        });
        $httpBackend
        .expect('GET', model.url)
        .respond(200, {
            items: 'hi'
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });

    it('should load even the template is not valid', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                template: '"type":"h1", "html":"{{title}}"}',

        };
        $widget.compile(model)
        .then(function(widget){
            expect(widget.getElementAttribute('id')).toBe(model.id);
            widget.on('load', function(){
                expect(widget.getChildren().length).toBe(0);
                done();
            });
        });
        $httpBackend
        .expect('GET', model.url)
        .respond(200, {
            items: [{
                title: 'text1'
            },{
                title: 'text2'
            },{
                title: 'text3'
            }]
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });

    it('should support runtime url changes', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                template: '{"type":"h1", "html":"{{title}}"}',

        };
        var newUrl = '/api/v2/test/persons';
        $widget.compile(model)
        .then(function(widget){
            function load1(){
                widget.off('load', load1);
                widget.on('load', load2);
                expect(widget.getChildren().length).toBe(3);
                widget.setProperty('url', newUrl);
            }
            
            function load2(){
                expect(widget.getChildren().length).toBe(3);
                done();
            }
            widget.on('load', load1);
            expect(widget.getElementAttribute('id')).toBe(model.id);
        });
        $httpBackend
        .expect('GET', model.url)
        .respond(200, {
            items: [{
                title: 'text1'
            },{
                title: 'text2'
            },{
                title: 'text3'
            }]
        });
        $httpBackend
        .expect('GET', newUrl)
        .respond(200, {
            items: [{
                title: 'text1'
            },{
                title: 'text2'
            },{
                title: 'text3'
            }]
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });
    
    it('should support just runtime url changes', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                template: '{"type":"h1", "html":"{{title}}"}',
                
        };
        var newUrl = '/api/v2/test/persons';
        $widget.compile(model)
        .then(function(widget){
            expect(widget.getNextPageIndex()).toBe(1);
            expect(widget.getCurrentPage()).toBe(0);
            expect(widget.getItemPerPage()).toBe(0);
            expect(widget.getPagesCount()).toBe(0);
            expect(widget.getState()).toBe('ideal');
            
            function load1(){
                widget.off('load', load1);
                widget.on('load', load2);
                widget.setProperty('url', newUrl);
            }
            
            function load2(){
                expect(widget.getChildren().length).toBe(3);
                done();
                
            }

            widget.on('load', load1);
        });
        $httpBackend
        .expect('GET', newUrl)
        .respond(200, {
            items: [{
                title: 'text1'
            },{
                title: 'text2'
            },{
                title: 'text3'
            }]
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });
    
    it('should support next page for empty list', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                template: '{"type":"h1", "html":"{{title}}"}',
                
        };
        $widget.compile(model)
        .then(function(widget){
            widget.on('load', function(){
                expect(widget.getChildren().length).toBe(3);
                expect(widget.getNextPageIndex()).toBe(2);
                expect(widget.getCurrentPage()).toBe(1);
                expect(widget.getState()).toBe('ideal');
                expect(widget.getItemPerPage()).toBe(3);
                widget.loadNextPage();
                expect(widget.getChildren().length).toBe(3);
                done();
            });
            expect(widget.getElementAttribute('id')).toBe(model.id);
        });
        $httpBackend
        .expect('GET', model.url)
        .respond(200, {
            items: [{
                title: 'text1'
            },{
                title: 'text2'
            },{
                title: 'text3'
            }],
            current_page: 1,
            page_number: 1,
            items_per_page: 3
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });
    
    it('should support next page list', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                template: '{"type":"h1", "html":"{{title}}"}',
                
        };
        $widget.compile(model)
        .then(function(widget){
            function load1(){
                widget.off('load', load1);
                widget.on('load', load2);
                expect(widget.getChildren().length).toBe(3);
                expect(widget.getNextPageIndex()).toBe(2);
                expect(widget.getCurrentPage()).toBe(1);
                expect(widget.getState()).toBe('ideal');
                expect(widget.getItemPerPage()).toBe(3);
                expect(widget.getPagesCount()).toBe(2);
                expect(widget.hasMorePage()).toBe(true);
                widget.loadNextPage();
                widget.loadNextPage();
            }
            
            function load2(){
                expect(widget.getChildren().length).toBe(6);
                expect(widget.getNextPageIndex()).toBe(3);
                expect(widget.getCurrentPage()).toBe(2);
                expect(widget.getState()).toBe('ideal');
                expect(widget.getItemPerPage()).toBe(3);
                expect(widget.hasMorePage()).toBe(false);
                expect(widget.getPagesCount()).toBe(2);
                widget.loadNextPage();
                done();
            }
            widget.on('load', load1);
        });
        $httpBackend
        .expect('GET', model.url)
        .respond(200, {
            items: [{
                title: 'text1'
            },{
                title: 'text2'
            },{
                title: 'text3'
            }],
            current_page: 1,
            page_number: 2,
            items_per_page: 3
        });
        $httpBackend
        .expect('GET', model.url+'?_px_p=2')
        .respond(200, {
            items: [{
                title: 'text4'
            },{
                title: 'text5'
            },{
                title: 'text6'
            }],
            current_page: 2,
            page_number: 2,
            items_per_page: 3
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });
    
    it('should support filters from model', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                filters:[{
                    key:'id',
                    value: '1'
                }],
                template: '{"type":"h1", "html":"{{title}}"}',
        };
        $widget.compile(model)
        .then(function(widget){
            function load1(){
                widget.off('load', load1);
                expect(widget.getChildren().length).toBe(3);
                done();
            }
            widget.on('load', load1);
        });
        $httpBackend
        .expect('GET', model.url+'?_px_fk%5B%5D=id&_px_fv%5B%5D=1')
        .respond(200, {
            items: [{
                title: 'text4'
            },{
                title: 'text5'
            },{
                title: 'text6'
            }],
            current_page: 1,
            page_number: 1,
            items_per_page: 3
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });
    it('should support sorts from model', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                sorts:[{
                    key:'id',
                    order: 'a'
                }],
                template: '{"type":"h1", "html":"{{title}}"}',
        };
        $widget.compile(model)
        .then(function(widget){
            function load1(){
                widget.off('load', load1);
                expect(widget.getChildren().length).toBe(3);
                done();
            }
            widget.on('load', load1);
        });
        $httpBackend
        .expect('GET', model.url+'?_px_sk%5B%5D=id&_px_so%5B%5D=a')
        .respond(200, {
            items: [{
                title: 'text4'
            },{
                title: 'text5'
            },{
                title: 'text6'
            }],
            current_page: 1,
            page_number: 1,
            items_per_page: 3
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });
    
    it('should support runtime sorts changes', function (done) {
        var model = {
                type: 'ObjectCollection',
                id: 'obj1',
                url: '/api/v2/test/books',
                template: '{"type":"h1", "html":"{{title}}"}',
        };
        $widget.compile(model)
        .then(function(widget){
            function load1(){
                widget.off('load', load1);
                widget.on('load', load2)
                expect(widget.getChildren().length).toBe(3);
                widget.setProperty('sorts', [{
                    key:'id',
                    order: 'a'
                }]);
            }
            function load2(){
                expect(widget.getChildren().length).toBe(3);
                done();
            }
            widget.on('load', load1);
        });
        $httpBackend
        .expect('GET', model.url)
        .respond(200, {
            items: [{
                title: 'text4'
            },{
                title: 'text5'
            },{
                title: 'text6'
            }],
            current_page: 1,
            page_number: 1,
            items_per_page: 3
        });
        $httpBackend
        .expect('GET', model.url+'?_px_sk%5B%5D=id&_px_so%5B%5D=a')
        .respond(200, {
            items: [{
                title: 'text4'
            },{
                title: 'text5'
            },{
                title: 'text6'
            }],
            current_page: 1,
            page_number: 1,
            items_per_page: 3
        });
        $rootScope.$apply();
        expect($httpBackend.flush).not.toThrow();
    });
});
